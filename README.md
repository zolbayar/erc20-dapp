---
component: Home
title: Build a Token
---

# ERC20 Token Generator - DAPP
Standart Smart contract Generator for Internal use


Code created using [VuePress](https://vuepress.vuejs.org/).

## Smart Contracts Source
 
Discover Smart Contracts source [here](https://gitlab.com/zolbayar/karvuon-erc20).

## Install dependencies

```bash
npm install
```

## Run DEV server

```bash
npm run dev
```

## Build dist

```bash
npm run build
```

## Deploy to gh-pages branch

```bash
npm run deploy
```

## License

Code released under the [MIT License](https://www.gitlab.com/zolbayar/erc20-dapp/blob/master/LICENSE).
