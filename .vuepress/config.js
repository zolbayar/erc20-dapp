module.exports = {
  title: 'Standart ERC20 for Karvuon',
  description: 'Standart smart contract generator',
  base: '/erc20-dapp/',
  head: [
    ['link', { rel: 'icon', href: '/favicon.ico' }],
    ['meta', { property: 'og:type', content: 'website' }],
    ['meta', { property: 'og:url', content: 'https://www.gitlab.com/zolbayar/erc20-dapp' }],
    ['meta', { property: 'og:image', content: 'https://www.gitlab.com/zolbayar/erc20-dapp/assets/images/erc20-token-generator.png' }], // eslint-disable-line max-len
    ['meta', { property: 'twitter:card', content: 'summary_large_image' }],
    ['meta', { property: 'twitter:image', content: 'https://www.gitlab.com/zolbayar/erc20-dapp/assets/images/erc20-token-generator.png' }], // eslint-disable-line max-len
    ['meta', { property: 'twitter:title', content: 'ERC20 Dapp' }],
    ['script', { src: 'assets/js/web3.min.js' }],
  ],
  plugins: [
  ],
  defaultNetwork: 'mainnet',
};
